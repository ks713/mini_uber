package com.example.demo;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.Optional;

@Service
public class RidesService {
    @Autowired
    private RidesRepository repo;
    @Transactional
    public void saveRide(Rides r){
        repo.save(r);
    }
    @Transactional
    public Optional<Rides> findRide(long id){
        return repo.findById(id);
    }
    @Transactional
    public List<Rides> findRideByUserID(long id){
       List<Rides> rlist=repo.findAll();
       List<Rides> rhis=new ArrayList<>();
       for(Rides r: rlist){
           if(r.getStarterID()==id){
               rhis.add(r);
           }
       }
       return rhis;
    }
}
