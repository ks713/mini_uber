package com.example.demo;

import jakarta.persistence.*;

@Entity
@Table(name = "Driver")
public class Driver {
    @Id
    private Long userID;
    private String licenseid;
    private String cartype;
    private String carplatenumber;
    public void setUserID(Long id){
        this.userID=id;
    }
    public void setLicenseid(String id){
        this.licenseid=id;
    }
    public void setCartype(String type){
        this.cartype=type;
    }
    public void setCarplatenumber(String number){
        this.carplatenumber=number;
    }

}
