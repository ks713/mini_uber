package com.example.demo;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class DriverService {
    @Autowired
    private DriverRepository driverRepository;
    @Transactional
    public void saveDriver(Driver driver){
        driverRepository.save(driver);
    }
    @Transactional
    public Optional<Driver> findDriver(Long id){
        return driverRepository.findById(id);
    }
}