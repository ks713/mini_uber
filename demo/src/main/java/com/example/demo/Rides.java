package com.example.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Rides {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long rideid;
    private String startLocation;
    private String endLocation;
    private int passnum;
    private long starterID;
    public long getRideid() {
        return rideid;
    }
    public String getStartLocation() {
        return startLocation;
    }
    public String getEndLocation() {
        return endLocation;
    }
    public int getPassnum() {
        return passnum;
    }
    public long getStarterID() {
        return starterID;
    }
    public void setRideid(long id){
        this.rideid=id;
    }
    public void setStartLocation(String location){
        this.startLocation=location;
    }
    public void setEndLocation(String location){
        this.endLocation=location;
    }
    public void setPassnum(int num){
        this.passnum=num;
    }
    public void setStarterID(long starterID) {
        this.starterID = starterID;
    }
}
