package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class customAuthenticate implements AuthenticationManager {
    @Autowired
    private UserService service;
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        // Perform custom authentication logic
        UberUser user = service.findUserByName(username);
        if (user != null && user.getPassword().equals(password)) {
            // Authentication successful
            return new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList());
        } else {
            // Authentication failed
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
