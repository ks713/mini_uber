package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
//exclude = { SecurityAutoConfiguration.class}
import org.springframework.context.annotation.Import;
@SpringBootApplication
@Import(SecurityConfig.class)
public class DemoApplication {
    public static void main(String[] args) {
       System.out.println("hello springboot entry");
        SpringApplication.run(DemoApplication.class, args);
    }
}
