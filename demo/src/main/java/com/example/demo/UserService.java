package com.example.demo;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Transactional
    public void saveUser(UberUser user) {
        System.out.println("NULL?????????");
        userRepository.save(user);
    }
    @Transactional
    public Optional<UberUser> findUser(Integer id){
        return userRepository.findById(id);
    }
    @Transactional
    public boolean contains(UberUser user){
        List<UberUser> alluser=userRepository.findAll();
        boolean contain=false;
        for(UberUser userone:alluser){
            if(userone.getUsername().equals(user.getUsername())){
                    contain=true;
                    break;
            }
        }
        return contain;
    }
    @Transactional
    public UberUser findUserByName(String name){
        for(UberUser user: userRepository.findAll()){
            if(user.getUsername().equals(name)){
                return user;
            }
        }
        return null;
    }
}
