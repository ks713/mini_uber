package com.example.demo;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
@Controller
public class DemoController {
    @Autowired
    private UserService service;
    @Autowired
    private DriverService driverService;
    @Autowired
    private RidesService ridesService;
    @Autowired
    private AuthenticationManager  authenticationManager;
    @RequestMapping(value = "/index",method=RequestMethod.GET)
    public String index() {
        System.out.println("index is called!!!!!!!!!!!!!!!!");
        return "index";
    }
    @RequestMapping(value = "/User",method=RequestMethod.POST)
    public String processindex(HttpServletRequest request, Model model){
        System.out.println("process index is called!!!!!!!!!!!!!!");
     UberUser si=new UberUser();
     si.setUsername(request.getParameter("username"));
     si.setPassword(request.getParameter("password"));
     System.out.println("bibibibibibibibibibibbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
        try {
            // Create an authentication token with the provided username and password
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(si.getUsername(), si.getPassword())
            );
            System.out.println("authenticate is called!!!!!");

            // Set the authenticated user in the security context
            System.out.println(authentication.isAuthenticated());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            System.out.println("no error!!!!!!!!!!");
            System.out.println(authentication.isAuthenticated());
            // Continue with your logic
            // ...
            return "rides.html";
        } catch (AuthenticationException e) {
            // Authentication failed
            // Handle the case where the user's credentials are not valid
            System.out.println("log in error!!!!!!!");
            return "redirect:/index/false";
        }
    }
    @RequestMapping(value = "/riderregister",method=RequestMethod.GET)
    public String register() {
        return "riderregister";
    }
    @RequestMapping(value = "/riderregister/false",method=RequestMethod.GET)
    public String registerAgain() {
        return "riderregister";
    }
    @RequestMapping(value = "/ridesget",method=RequestMethod.GET)
    public String ridesT() {
         System.out.println("rides called eeeeeeee!!!!!!!!");
        return "rides";
    }
    @RequestMapping(value = "/Register",method=RequestMethod.POST)
    public String registerAccount(HttpServletRequest request){
        UberUser si=new UberUser();
        System.out.println("register is called!!!!!!");
        si.setUsername(request.getParameter("username"));
        si.setPassword(request.getParameter("password"));
        String cartype=request.getParameter("cartype");
        String carplatenumber=request.getParameter("carplatenumber");
        String licenseid=request.getParameter("licenseid");
        System.out.println("Are there any errors?!!!!!!");
        if(cartype.length()==0&&carplatenumber.length()==0&&licenseid.length()==0){
           si.setregistered(false);
        }
        if(!service.contains(si)) {
            service.saveUser(si);
        }else{
            return "redirect:/riderregister/false";
        }
        Driver driver=new Driver();
        driver.setUserID(si.getUserID());
        driver.setCartype(cartype);
        driver.setCarplatenumber(carplatenumber);
        driver.setLicenseid(licenseid);
        driverService.saveDriver(driver);
        return "redirect:/index";
    }
    @RequestMapping(value = "/ridespost",method=RequestMethod.POST)
    public String rideprocess(HttpServletRequest request,Model model){
        System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
        System.out.println(model.getAttribute("urname"));
        System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
        String fromlocation=request.getParameter("from");
        String tolocation=request.getParameter("to");
        String passnum=request.getParameter("passnum");
        int passnumber=Integer.parseInt(passnum);
        Rides rides=new Rides();
        rides.setPassnum(passnumber);
        rides.setEndLocation(tolocation);
        rides.setStartLocation(fromlocation);
        rides.setStarterID(0);
        ridesService.saveRide(rides);
        return "rides";
    }
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String getLogIN(){
        return "index";
    }
    @RequestMapping(value = "/index/false",method=RequestMethod.GET)
    public String falseLogIN(){
        System.out.println("index false called!!!!!!!!");
        return "index";
    }
}