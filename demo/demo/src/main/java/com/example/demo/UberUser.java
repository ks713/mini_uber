package com.example.demo;


import jakarta.persistence.*;

@Entity
@Table(name = "Users")
public class UberUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userID;
    private String username;
    private String password;
    private boolean driverregistered=false;
    private boolean enabled=true;
    public Long getUserID(){
        return userID;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password=password;
    }
    public void setregistered(boolean regsitered){
        this.driverregistered=regsitered;
    }
}
